var packager = require('electron-packager')
var fs = require('fs-extra')
var options = 
{
	dir : './',
	asar : true,
	prune : true,
	arch : 'x64',
	platform : 'win32',
	overwrite : true,
	out : './release-builds',
	win32metadata : 
	{
		CompanyName : 'JivyGroup',
		FileDescription : 'Starts 10Trader app',
		ProductName: '10Trader'
	},
	'app-copyright' : '@JivyGroup 2017'
}

packager(options, function done_callback (err, appPaths)
{
	fs.copy('./resources','./release-builds/10Trader-win32-x64/resources',()=> console.log('done'));
})