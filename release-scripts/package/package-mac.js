var packager = require('electron-packager')
var fs = require('fs-extra')
var options = 
{
	dir : './',
	asar : true,
	prune : true,
	arch : 'x64',
	platform : 'darwin',
	overwrite : true,
	out : './release-builds',
	'app-copyright' : '@JivyGroup 2017'
}

packager(options, function done_callback (err, appPaths)
{
	fs.copy('./resources','./release-builds/10Trader-darwin-x64/10Trader.app/Contents/Resources',()=> console.log('done'));
})