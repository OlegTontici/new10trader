const { app} = require('electron');
const { BrowserWindow} = require('electron');
var mainWindow = null;
 //handle setupevents as quickly as possible
const setupEvents = require('./release-scripts/setupEvents')
if (setupEvents.handleSquirrelEvent()) 
{
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

app.on('ready',function()
{
    mainWindow = new BrowserWindow({width:1024,height:768, frame:true})
    mainWindow.on('closed',()=>
    {
        app.exit();
    })
    mainWindow.loadURL('file://' + __dirname + '/index.html');
    mainWindow.webContents.openDevTools();   

    mainWindow.setMenuBarVisibility(false);
    mainWindow.setAutoHideMenuBar(true);
    Update();
})

function Update()
{
    const package = require('./package.json');
    const autoUpdater = require("electron").autoUpdater;

    autoUpdater.on('update-availabe', () => 
    {
        console.log('update available');
    });

    autoUpdater.on('checking-for-update', () => 
    {
        console.log('checking-for-update');
    });

    autoUpdater.on('update-not-available', () => 
    {
        console.log('update-not-available');
    });

    autoUpdater.on('update-downloaded', (e) => 
    {
        console.log(e)
        autoUpdater.quitAndInstall();
    });   
  
    if(process.env.NODE_ENV != 'development')
    {
        var updateUrl = process.platform == 'win32' ? 'https://s3-eu-west-1.amazonaws.com/download.10trader.com/installers/Updates/' : '';
        autoUpdater.setFeedURL(updateUrl);
        autoUpdater.checkForUpdates();
    }    
}