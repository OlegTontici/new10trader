import { Subject,Observable } from 'rxjs'
import { Injectable } from '@angular/core'
import { ProviderConfigurationModel } from '../Models/ProviderConfiguration.model'

@Injectable()
export class DataFlowManager
{
    public  SetAppConfigs : Subject<ProviderConfigurationModel> = new Subject<ProviderConfigurationModel>();
    public  ApplicationConfigurationsStream : Observable<ProviderConfigurationModel> = new Observable<ProviderConfigurationModel>();

    public ServiceInitializedStream : Subject<any> = new Subject<any>();
    public AllServicesInitializedStream : Observable<any> = this.ServiceInitializedStream.bufferCount(2);

    constructor()
    {
        this.ApplicationConfigurationsStream = this.SetAppConfigs.publishReplay(1).refCount();;
    }
}