"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var IThemesManager = (function () {
    function IThemesManager() {
    }
    /**
    * Try to find theme for specific component in resources/OperatorName/css folder and to apply it to given component.
    * >Should be invoked by any component that wants to apply operator specific theme in AfterContentInit interface method implementation
    * @param componentName - name of the component to searach theme for and ApplyAllExistingThemes
    *
    */
    IThemesManager.prototype.ApplyThemeForComponent = function (componentName) {
    };
    return IThemesManager;
}());
IThemesManager = __decorate([
    core_1.Injectable()
], IThemesManager);
exports.IThemesManager = IThemesManager;
//# sourceMappingURL=IThemes.manager.js.map