import { Injectable } from '@angular/core'
import { ICssFilesInjectorService } from '../../Services/CssFilesInjectorService/ICssFilesInjector.service'
import { FileSystem } from '../../Helpers/FileSystem.helper'

import { IThemesManager } from './IThemes.manager'
import { DataFlowManager } from '../../Managers/DataFlow.manager'

@Injectable()
export class ThemesManager implements IThemesManager
{
    private operatorHasOwnTheme : boolean ;
    private operatorName : string;
    
    constructor(private cssInjector : ICssFilesInjectorService,private dataManager : DataFlowManager) 
    {
        this.dataManager.ApplicationConfigurationsStream.subscribe(configs =>
        {
            this.operatorHasOwnTheme = configs.HasOwnTheme;
            this.operatorName = configs.OperatorName;
        });      
    }

    public ApplyThemeForComponent(componentName : string) : void
    {
        if(this.operatorHasOwnTheme)
        {
            let fileName = componentName + `.component.${this.operatorName}.css`;
            this.cssInjector.Inject(fileName,this.operatorName,componentName);
        }
    }
}