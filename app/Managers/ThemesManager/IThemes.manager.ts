import { Injectable } from '@angular/core'

@Injectable()
export abstract class IThemesManager
{     
     /**
     * Try to find theme for specific component in resources/OperatorName/css folder and to apply it to given component.
     * >Should be invoked by any component that wants to apply operator specific theme in AfterContentInit interface method implementation
     * @param componentName - name of the component to searach theme for and ApplyAllExistingThemes
     * 
     */ 
    public ApplyThemeForComponent(componentName : string) : void
    {
       
    }
}