"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ICssFilesInjector_service_1 = require("../../Services/CssFilesInjectorService/ICssFilesInjector.service");
var DataFlow_manager_1 = require("../../Managers/DataFlow.manager");
var ThemesManager = (function () {
    function ThemesManager(cssInjector, dataManager) {
        var _this = this;
        this.cssInjector = cssInjector;
        this.dataManager = dataManager;
        this.dataManager.ApplicationConfigurationsStream.subscribe(function (configs) {
            _this.operatorHasOwnTheme = configs.HasOwnTheme;
            _this.operatorName = configs.OperatorName;
        });
    }
    ThemesManager.prototype.ApplyThemeForComponent = function (componentName) {
        if (this.operatorHasOwnTheme) {
            var fileName = componentName + (".component." + this.operatorName + ".css");
            this.cssInjector.Inject(fileName, this.operatorName, componentName);
        }
    };
    return ThemesManager;
}());
ThemesManager = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [ICssFilesInjector_service_1.ICssFilesInjectorService, DataFlow_manager_1.DataFlowManager])
], ThemesManager);
exports.ThemesManager = ThemesManager;
//# sourceMappingURL=Themes.manager.js.map