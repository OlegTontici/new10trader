import { ProviderConfigurationModel } from './ProviderConfiguration.model'

export class ProviderConfigurationsModel
{
    public ProviderLiveConfiguration : ProviderConfigurationModel = new ProviderConfigurationModel();
    public ProviderDemoConfiguration : ProviderConfigurationModel = new ProviderConfigurationModel();
}