import { LightStreamerConnectionDataModel } from './Lightstreamer/LightStreamerConnectionData.model'
import { PlatformApiConnectionDataModel } from './PlatformApi/PlatformApiConnectionData.model'

export class ProviderConfigurationModel
{
    public LighStreamerConnectionData : LightStreamerConnectionDataModel = new LightStreamerConnectionDataModel();
    public PlatformApiConnectionData  : PlatformApiConnectionDataModel = new PlatformApiConnectionDataModel();    
    public OperatorId : string;
    public OperatorName : string;
    public HasOwnTheme : boolean;        
}