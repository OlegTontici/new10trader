import { EOptionType } from './Enums/OptionType.enum'
import { ETradeActionType } from './Enums/TradeActionType.enum'

export class BuyRequestObject
{
    public sessionID : string;
    public optionInstanceID : number;
    public tradeActionType : ETradeActionType;
    public amount : number;
    public strike : number;
    public optionType : EOptionType;
    public clientType : number = 4;
    public additionalStrike : number;
    public subOptionInstanceID : number;
    public lotPremium : number;
    public lotAmount : number;
    public secondSubOptionInstanceID : number;
}

export class CancelTradeActionRequestObject
{
    public sessionID :string;
    public tradeActionID : number;
    public cancelationFee : number;
    public clientDateTime : string;
}

export class GetTokenRequestObject
{
    public sessionID : string;
}

export class GetTraderBalanceRequestObject
{
    public sessionID : string;
    public returnWithBonus : boolean;
    public campaignName : string;
}

export class LoginRequestObject
{
    public username : string;
    public password : string;
    public operatorName : string;
    public version : string;
    public clientApplication : string;
}

export class GetTraderParamsRequestObject
{
    public sessionID : string;
}

export class LogoutRequestObject
{
    public sessionID : string;
}

export class QuickDemoLoginRequestObject
{
    public operatorName : string;
    public version : string;
    public clientApplication : string;
    public IPAddress : string;
    public currency : string;
}

export class SellRequestObject
{
    public sessionID : string;
    public tradeActionID : number;
    public sellPrice : number;
}

export class GetReportRequestObject
{
    public sessionID : string;
    public reportName : string;
    public columns : string[];
    public filters : ReportFilter[];
    public sortColumn : string;
    public sortDirection : string;
    public top : number;
    public skip : number;
    public distinctColumn : string;
}

export class IsOperatorSiteActiveRequestObject
{
   constructor(public operatorName : string)
   {
       
   }
}

export class GetRecentHistoricMarketDataRequestObject
{
    public sessionID :string;
    public assetTicker : string;
    public candlesLimit : number;
    public periodString : string;
}

export class GetTradeActionCancellationInformationRequestObject
{
    public sessionID : string;
    public tradeActionID : number;
}

export class GetProviderSettingsRequestObject
{
    constructor(public OperatorId : string)
    {
        
    } 
}

//helpers
export class ReportFilter
{
    constructor(column : string,operator : string,value: string)
    {
        this.filterColumn = column;
        this.filterOperator = operator;
        this.filterValue = value;
    }
    public filterColumn : string;
    public filterOperator : string;
    public filterValue : string;
}