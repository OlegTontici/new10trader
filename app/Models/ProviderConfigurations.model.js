"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProviderConfiguration_model_1 = require("./ProviderConfiguration.model");
var ProviderConfigurationsModel = (function () {
    function ProviderConfigurationsModel() {
        this.ProviderLiveConfiguration = new ProviderConfiguration_model_1.ProviderConfigurationModel();
        this.ProviderDemoConfiguration = new ProviderConfiguration_model_1.ProviderConfigurationModel();
    }
    return ProviderConfigurationsModel;
}());
exports.ProviderConfigurationsModel = ProviderConfigurationsModel;
//# sourceMappingURL=ProviderConfigurations.model.js.map