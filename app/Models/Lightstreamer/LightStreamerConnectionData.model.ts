export class LightStreamerConnectionDataModel
{    
    public  LSServerURL : string;
    public  LSAdapterSet : string;    
    public  LSSubscriptionMode : string;
    public  LSItems : string[];
    public  LSFields : string[];
}