"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BuyRequestObject = (function () {
    function BuyRequestObject() {
        this.clientType = 4;
    }
    return BuyRequestObject;
}());
exports.BuyRequestObject = BuyRequestObject;
var CancelTradeActionRequestObject = (function () {
    function CancelTradeActionRequestObject() {
    }
    return CancelTradeActionRequestObject;
}());
exports.CancelTradeActionRequestObject = CancelTradeActionRequestObject;
var GetTokenRequestObject = (function () {
    function GetTokenRequestObject() {
    }
    return GetTokenRequestObject;
}());
exports.GetTokenRequestObject = GetTokenRequestObject;
var GetTraderBalanceRequestObject = (function () {
    function GetTraderBalanceRequestObject() {
    }
    return GetTraderBalanceRequestObject;
}());
exports.GetTraderBalanceRequestObject = GetTraderBalanceRequestObject;
var LoginRequestObject = (function () {
    function LoginRequestObject() {
    }
    return LoginRequestObject;
}());
exports.LoginRequestObject = LoginRequestObject;
var GetTraderParamsRequestObject = (function () {
    function GetTraderParamsRequestObject() {
    }
    return GetTraderParamsRequestObject;
}());
exports.GetTraderParamsRequestObject = GetTraderParamsRequestObject;
var LogoutRequestObject = (function () {
    function LogoutRequestObject() {
    }
    return LogoutRequestObject;
}());
exports.LogoutRequestObject = LogoutRequestObject;
var QuickDemoLoginRequestObject = (function () {
    function QuickDemoLoginRequestObject() {
    }
    return QuickDemoLoginRequestObject;
}());
exports.QuickDemoLoginRequestObject = QuickDemoLoginRequestObject;
var SellRequestObject = (function () {
    function SellRequestObject() {
    }
    return SellRequestObject;
}());
exports.SellRequestObject = SellRequestObject;
var GetReportRequestObject = (function () {
    function GetReportRequestObject() {
    }
    return GetReportRequestObject;
}());
exports.GetReportRequestObject = GetReportRequestObject;
var IsOperatorSiteActiveRequestObject = (function () {
    function IsOperatorSiteActiveRequestObject(operatorName) {
        this.operatorName = operatorName;
    }
    return IsOperatorSiteActiveRequestObject;
}());
exports.IsOperatorSiteActiveRequestObject = IsOperatorSiteActiveRequestObject;
var GetRecentHistoricMarketDataRequestObject = (function () {
    function GetRecentHistoricMarketDataRequestObject() {
    }
    return GetRecentHistoricMarketDataRequestObject;
}());
exports.GetRecentHistoricMarketDataRequestObject = GetRecentHistoricMarketDataRequestObject;
var GetTradeActionCancellationInformationRequestObject = (function () {
    function GetTradeActionCancellationInformationRequestObject() {
    }
    return GetTradeActionCancellationInformationRequestObject;
}());
exports.GetTradeActionCancellationInformationRequestObject = GetTradeActionCancellationInformationRequestObject;
var GetProviderSettingsRequestObject = (function () {
    function GetProviderSettingsRequestObject(OperatorId) {
        this.OperatorId = OperatorId;
    }
    return GetProviderSettingsRequestObject;
}());
exports.GetProviderSettingsRequestObject = GetProviderSettingsRequestObject;
//helpers
var ReportFilter = (function () {
    function ReportFilter(column, operator, value) {
        this.filterColumn = column;
        this.filterOperator = operator;
        this.filterValue = value;
    }
    return ReportFilter;
}());
exports.ReportFilter = ReportFilter;
//# sourceMappingURL=Requests.js.map