import { ProviderConfigurationsModel } from './ProviderConfigurations.model'

export class AppContextModel
{
    constructor()
    {
        this.IsDemo = false;
    }
    public ProviderSettingsApiUrl : string;
    public OperatorLiveId : string;
    public IsDemo : boolean ;
}