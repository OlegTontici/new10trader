"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetTokenResponseObject = (function () {
    function GetTokenResponseObject() {
    }
    return GetTokenResponseObject;
}());
exports.GetTokenResponseObject = GetTokenResponseObject;
var LoginResponseObject = (function () {
    function LoginResponseObject() {
    }
    return LoginResponseObject;
}());
exports.LoginResponseObject = LoginResponseObject;
var QuickDemoLoginResponseObject = (function () {
    function QuickDemoLoginResponseObject() {
    }
    return QuickDemoLoginResponseObject;
}());
exports.QuickDemoLoginResponseObject = QuickDemoLoginResponseObject;
var GetTraderParamsResponseObject = (function () {
    function GetTraderParamsResponseObject() {
    }
    return GetTraderParamsResponseObject;
}());
exports.GetTraderParamsResponseObject = GetTraderParamsResponseObject;
var GetReportResponseObject = (function () {
    function GetReportResponseObject() {
    }
    return GetReportResponseObject;
}());
exports.GetReportResponseObject = GetReportResponseObject;
var GetTraderBalanceResponseObject = (function () {
    function GetTraderBalanceResponseObject() {
    }
    return GetTraderBalanceResponseObject;
}());
exports.GetTraderBalanceResponseObject = GetTraderBalanceResponseObject;
var IsOperatorSiteActiveResponseObject = (function () {
    function IsOperatorSiteActiveResponseObject() {
    }
    return IsOperatorSiteActiveResponseObject;
}());
exports.IsOperatorSiteActiveResponseObject = IsOperatorSiteActiveResponseObject;
var GetRecentHistoricMarketDataResponseObject = (function () {
    function GetRecentHistoricMarketDataResponseObject() {
    }
    return GetRecentHistoricMarketDataResponseObject;
}());
exports.GetRecentHistoricMarketDataResponseObject = GetRecentHistoricMarketDataResponseObject;
var GetTradeActionCancellationInformationResponseObject = (function () {
    function GetTradeActionCancellationInformationResponseObject() {
    }
    return GetTradeActionCancellationInformationResponseObject;
}());
exports.GetTradeActionCancellationInformationResponseObject = GetTradeActionCancellationInformationResponseObject;
var GenericResponse = (function () {
    function GenericResponse() {
    }
    Object.defineProperty(GenericResponse.prototype, "HasErrors", {
        get: function () {
            return this.errors.length > 0;
        },
        set: function (v) {
            this._HasErrors = v;
        },
        enumerable: true,
        configurable: true
    });
    return GenericResponse;
}());
exports.GenericResponse = GenericResponse;
var BuyResponseObject = (function () {
    function BuyResponseObject() {
    }
    return BuyResponseObject;
}());
exports.BuyResponseObject = BuyResponseObject;
/* helpers */
var BalanceInformation = (function () {
    function BalanceInformation() {
    }
    return BalanceInformation;
}());
var ReportRow = (function () {
    function ReportRow() {
    }
    return ReportRow;
}());
var ReportSummary = (function () {
    function ReportSummary() {
    }
    return ReportSummary;
}());
var HistoryRow = (function () {
    function HistoryRow() {
    }
    return HistoryRow;
}());
var ErrorMessage = (function () {
    function ErrorMessage() {
    }
    return ErrorMessage;
}());
//# sourceMappingURL=Responses.js.map