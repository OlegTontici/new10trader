"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EErrorSell;
(function (EErrorSell) {
    EErrorSell[EErrorSell["AutInvalidSessionID"] = 5] = "AutInvalidSessionID";
    EErrorSell[EErrorSell["TAInvalidOptionInstance"] = 201] = "TAInvalidOptionInstance";
    EErrorSell[EErrorSell["TAExceedMaxMarginToAllowTrade"] = 213] = "TAExceedMaxMarginToAllowTrade";
    EErrorSell[EErrorSell["TAInvalidTradeActionID"] = 215] = "TAInvalidTradeActionID";
    EErrorSell[EErrorSell["TATradingNotAllowed"] = 216] = "TATradingNotAllowed";
    EErrorSell[EErrorSell["TASellPriceIsInvalid"] = 217] = "TASellPriceIsInvalid";
    EErrorSell[EErrorSell["TradeActionIsAlreadySold"] = 221] = "TradeActionIsAlreadySold";
    EErrorSell[EErrorSell["AccPlayerIsSuspended"] = 605] = "AccPlayerIsSuspended";
})(EErrorSell = exports.EErrorSell || (exports.EErrorSell = {}));
//# sourceMappingURL=ErrorSell.enum.js.map