"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EOptionType;
(function (EOptionType) {
    EOptionType[EOptionType["ChangingStrike"] = 1] = "ChangingStrike";
    EOptionType[EOptionType["OneTouchIntraDay"] = 2] = "OneTouchIntraDay";
    EOptionType[EOptionType["RangeHighLow"] = 3] = "RangeHighLow";
    EOptionType[EOptionType["FixedPayoutHighLow"] = 4] = "FixedPayoutHighLow";
    EOptionType[EOptionType["CompositeHighLow"] = 5] = "CompositeHighLow";
})(EOptionType = exports.EOptionType || (exports.EOptionType = {}));
//# sourceMappingURL=OptionType.enum.js.map