export enum ETradeActionType
{
    Put = 1,
    Call = 2,
    OneTouchAbove = 3,
    OneTouchBelow = 4,
    Range = 5,
    FixedPayoutPut = 6,
    FixedPayoutCall = 7,
    FixedStrikePut = 8,
    FixedStrikeCall = 9 
}