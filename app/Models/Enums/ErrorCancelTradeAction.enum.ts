export enum EErrorCancelTradeAction
{
    AutInvalidSessionID = 5,
    UnExpectedCancellationFee = 223,
    TAInvalidTradeActionID = 215,
    TradeActionNotCancellable = 222,
    TATradingNotAllowed = 216
}