"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ETradeActionType;
(function (ETradeActionType) {
    ETradeActionType[ETradeActionType["Put"] = 1] = "Put";
    ETradeActionType[ETradeActionType["Call"] = 2] = "Call";
    ETradeActionType[ETradeActionType["OneTouchAbove"] = 3] = "OneTouchAbove";
    ETradeActionType[ETradeActionType["OneTouchBelow"] = 4] = "OneTouchBelow";
    ETradeActionType[ETradeActionType["Range"] = 5] = "Range";
    ETradeActionType[ETradeActionType["FixedPayoutPut"] = 6] = "FixedPayoutPut";
    ETradeActionType[ETradeActionType["FixedPayoutCall"] = 7] = "FixedPayoutCall";
    ETradeActionType[ETradeActionType["FixedStrikePut"] = 8] = "FixedStrikePut";
    ETradeActionType[ETradeActionType["FixedStrikeCall"] = 9] = "FixedStrikeCall";
})(ETradeActionType = exports.ETradeActionType || (exports.ETradeActionType = {}));
//# sourceMappingURL=TradeActionType.enum.js.map