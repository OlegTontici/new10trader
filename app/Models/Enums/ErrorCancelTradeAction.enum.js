"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EErrorCancelTradeAction;
(function (EErrorCancelTradeAction) {
    EErrorCancelTradeAction[EErrorCancelTradeAction["AutInvalidSessionID"] = 5] = "AutInvalidSessionID";
    EErrorCancelTradeAction[EErrorCancelTradeAction["UnExpectedCancellationFee"] = 223] = "UnExpectedCancellationFee";
    EErrorCancelTradeAction[EErrorCancelTradeAction["TAInvalidTradeActionID"] = 215] = "TAInvalidTradeActionID";
    EErrorCancelTradeAction[EErrorCancelTradeAction["TradeActionNotCancellable"] = 222] = "TradeActionNotCancellable";
    EErrorCancelTradeAction[EErrorCancelTradeAction["TATradingNotAllowed"] = 216] = "TATradingNotAllowed";
})(EErrorCancelTradeAction = exports.EErrorCancelTradeAction || (exports.EErrorCancelTradeAction = {}));
//# sourceMappingURL=ErrorCancelTradeAction.enum.js.map