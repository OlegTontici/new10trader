export enum EErrorSell
{
    AutInvalidSessionID = 5,
    TAInvalidOptionInstance = 201,
    TAExceedMaxMarginToAllowTrade = 213,
    TAInvalidTradeActionID = 215,
    TATradingNotAllowed = 216,
    TASellPriceIsInvalid = 217,
    TradeActionIsAlreadySold = 221,
    AccPlayerIsSuspended = 605
}