export enum EOptionType
{
    ChangingStrike = 1,
    OneTouchIntraDay = 2,
    RangeHighLow = 3,
    FixedPayoutHighLow = 4,
    CompositeHighLow = 5 
}