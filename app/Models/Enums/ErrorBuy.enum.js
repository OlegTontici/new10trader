"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EErrorBuy;
(function (EErrorBuy) {
    EErrorBuy[EErrorBuy["AutInvalidSessionID"] = 5] = "AutInvalidSessionID";
    EErrorBuy[EErrorBuy["TAInvalidOptionInstance"] = 201] = "TAInvalidOptionInstance";
    EErrorBuy[EErrorBuy["TAExceedMaxTradeActions"] = 202] = "TAExceedMaxTradeActions";
    EErrorBuy[EErrorBuy["TAInvestmentOutOfRange"] = 203] = "TAInvestmentOutOfRange";
    EErrorBuy[EErrorBuy["TAExceedMaxProfits"] = 204] = "TAExceedMaxProfits";
    EErrorBuy[EErrorBuy["TAExceedMaxLosses"] = 205] = "TAExceedMaxLosses";
    EErrorBuy[EErrorBuy["TAAddTradeActionFailed"] = 206] = "TAAddTradeActionFailed";
    EErrorBuy[EErrorBuy["TANotEnoughMoney"] = 207] = "TANotEnoughMoney";
    EErrorBuy[EErrorBuy["TAInvestAmountTooHigh"] = 208] = "TAInvestAmountTooHigh";
    EErrorBuy[EErrorBuy["TAInvestAmountTooLow"] = 209] = "TAInvestAmountTooLow";
    EErrorBuy[EErrorBuy["TAExceedMaxTradeVolume"] = 210] = "TAExceedMaxTradeVolume";
    EErrorBuy[EErrorBuy["TAInvalidTradingStrike"] = 211] = "TAInvalidTradingStrike";
    EErrorBuy[EErrorBuy["TAExceedMaxTraderExposure"] = 212] = "TAExceedMaxTraderExposure";
    EErrorBuy[EErrorBuy["TAExceedMaxMarginToAllowTrade"] = 213] = "TAExceedMaxMarginToAllowTrade";
    EErrorBuy[EErrorBuy["TAMinMarginTimeToAllowTrade"] = 214] = "TAMinMarginTimeToAllowTrade";
    EErrorBuy[EErrorBuy["TAInvalidTradeActionID"] = 215] = "TAInvalidTradeActionID";
    EErrorBuy[EErrorBuy["TATradingNotAllowed"] = 216] = "TATradingNotAllowed";
    EErrorBuy[EErrorBuy["MaxLossPerDayExceeded"] = 218] = "MaxLossPerDayExceeded";
    EErrorBuy[EErrorBuy["AccPlayerIsSuspended"] = 506] = "AccPlayerIsSuspended";
    EErrorBuy[EErrorBuy["TradingNotAllowedCalendarException"] = 219] = "TradingNotAllowedCalendarException";
    EErrorBuy[EErrorBuy["ErrorCalculatingWinProbability"] = 220] = "ErrorCalculatingWinProbability";
    EErrorBuy[EErrorBuy["TraderLimitedByMaxDepositThreshold"] = 409] = "TraderLimitedByMaxDepositThreshold";
})(EErrorBuy = exports.EErrorBuy || (exports.EErrorBuy = {}));
//# sourceMappingURL=ErrorBuy.enum.js.map