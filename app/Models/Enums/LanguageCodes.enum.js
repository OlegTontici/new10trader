"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ELanguagesCodes;
(function (ELanguagesCodes) {
    ELanguagesCodes[ELanguagesCodes["None"] = 0] = "None";
    ELanguagesCodes[ELanguagesCodes["English"] = 1033] = "English";
    ELanguagesCodes[ELanguagesCodes["French"] = 1035] = "French";
    ELanguagesCodes[ELanguagesCodes["Japanese"] = 1041] = "Japanese";
    ELanguagesCodes[ELanguagesCodes["Italian"] = 1040] = "Italian";
    ELanguagesCodes[ELanguagesCodes["Chinese"] = 2052] = "Chinese";
    ELanguagesCodes[ELanguagesCodes["Arabic"] = 1025] = "Arabic";
})(ELanguagesCodes = exports.ELanguagesCodes || (exports.ELanguagesCodes = {}));
//# sourceMappingURL=LanguageCodes.enum.js.map