export enum ELanguagesCodes
{
    None = 0,
    English = 1033,
    French = 1035,
    Japanese = 1041,
    Italian = 1040,
    Chinese = 2052,
    Arabic = 1025
}