export class GetTokenResponseObject
{
    public token : string;
}

export class LoginResponseObject
{
    public sessionID : string;
    public traderID : string;
}

export class QuickDemoLoginResponseObject
{
    public sessionID : string;
    public traderID : string;
    public userName : string;
}

export class GetTraderParamsResponseObject
{
    public OperatorID : number;
    public FirstName : string;
    public LastName : string;
    public Email : string;
    public AccountType : string;
    public CurrencyID : number;
    public CustomVariables : any;
    public CurrencyCode : string;
    public LanguageID : number;
    public MaxInvestment : number;
    public MinInvestment : number;
    public MaxLotsInvestmen : number;
    public MinLotsInvestment : number;
    public MinDeposit : number;
    public MaxDeposit : number;
    public MinWithdraw : number; 
    public MaxWithdraw : number;
    public PurchaseSliderInterval : number;
    public PurchaseSliderMinValue : number;    
    public PurchaseSliderMaxValue : number;
    public PurchaseSuggestedAmounts : string;
    public PurchaseInvestmentDefaultValue : number;
    public LotSize : number;
    public PurchaseSliderIntervalLots : number;
    public PurchaseSliderMinValueLots : number;
    public PurchaseSliderMaxValueLots : number;
    public PurchaseSuggestedAmountsLots : string;
    public PurchaseInvestmentDefaultValueLots : number;

}

export class GetReportResponseObject
{
    public reportName : string;
    public results : ReportRow[];
    public sortColumn : string;
    public sortDirection : string;
    public summary : ReportSummary;
    public totalRows : number;

}

export class GetTraderBalanceResponseObject
{
    public balanceInformation : BalanceInformation;
}

export class IsOperatorSiteActiveResponseObject
{
    public isActive : boolean;
}

export class GetRecentHistoricMarketDataResponseObject
{
    public CandlesResponse : HistoryRow[];
}

export class GetTradeActionCancellationInformationResponseObject
{
    public CancellableUntil : string;
    public CancellationFee : number;
    public IsCancellable : boolean;
}

export class GenericResponse<TOut>
{
    public data : TOut;
    public status : string;
    public errors : ErrorMessage[];
    
    private _HasErrors : boolean;
    public get HasErrors() : boolean {
        return this.errors.length > 0;
    }
    public set HasErrors(v : boolean) {
        this._HasErrors = v;
    }
    
}

export class BuyResponseObject
{
    public TradeActionId : number;
}

/* helpers */
class BalanceInformation
{
    public balance : string;
    public bonusBalance : string;
    public BonusInfo : any[];
    public traderRetentionInformation : any;
}

class ReportRow
{
    public AdditionalStrike : number;
    public AssetName : string;
    public CloseStrike : string;
    public CloseTime : string;
    public Duration : number;
    public ExpirationType : string;
    public Lots : number;
    public LotSize : number;
    public MarketRate : number;
    public MoneyInvestment : number;
    public OptionInstanceExpirationTime : string;
    public OptionInstanceExpirationValue : number;
    public Premium : number;
    public Status : string;
    public TradeActionID : number;
    public TraderIncome : number;
    public TradeType : number;
    public TradingStrike : number;
    public TradingTime : string;
}

class ReportSummary
{
    public MoneyInvestment : number;
    public TraderIncome : number;
}

class HistoryRow
{
    public Open : number;
    public High : number;
    public Low : number;
    public Close : number;
    public StartTimeStampUtc : string;
    public LastTickTimeUtc : string;
    public EndTimeStampUtc : string;
}
class ErrorMessage
{
    public description : string;
    public errorCode : string;
    public errorinfo : string;
}
