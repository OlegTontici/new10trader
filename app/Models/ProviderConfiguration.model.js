"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LightStreamerConnectionData_model_1 = require("./Lightstreamer/LightStreamerConnectionData.model");
var PlatformApiConnectionData_model_1 = require("./PlatformApi/PlatformApiConnectionData.model");
var ProviderConfigurationModel = (function () {
    function ProviderConfigurationModel() {
        this.LighStreamerConnectionData = new LightStreamerConnectionData_model_1.LightStreamerConnectionDataModel();
        this.PlatformApiConnectionData = new PlatformApiConnectionData_model_1.PlatformApiConnectionDataModel();
    }
    return ProviderConfigurationModel;
}());
exports.ProviderConfigurationModel = ProviderConfigurationModel;
//# sourceMappingURL=ProviderConfiguration.model.js.map