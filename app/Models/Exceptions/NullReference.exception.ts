export class NullReferenceException extends Error
{
    constructor(errorMessage : string) 
    {
        super(errorMessage);   
    }
}