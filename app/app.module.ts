import { NgModule }      from '@angular/core'; 
import { BrowserModule } from '@angular/platform-browser'; 
import { HttpModule } from '@angular/http'
import { RouterModule,Router } from '@angular/router'


import { AppComponent }   from './app.component'; 
import { LoginWindowComponent } from './Components/LoginWindowComponent/LoginWindow.component'
import { MainWindowComponent} from './Components/MainWindowComponent/MainWindow.component'

import { IAppConfigurationService } from './Services/AppConfigurationsService/IAppConfigurations.service'
import { AppConfigurationService } from './Services/AppConfigurationsService/AppConfigurations.service'

import { ILoggerService } from './Services/LoggingService/ILogger.service'
import { LoggerService } from './Services/LoggingService/Logger.service'

import { IPlatformApiService } from './Services/PlatformApiService/IPlatformApi.service'
import { PlatformApiService } from './Services/PlatformApiService/PlatformApi.service'

import { LightstreamerSubscriptionService } from './Services/LightstreamerSubscriptionService/LightstreamerSubscription.service'
import { ILightstreamerSubscriptionService } from './Services/LightstreamerSubscriptionService/ILightstreamerSubscription.service'

import { CssFilesInjectorService } from './Services/CssFilesInjectorService/CssFilesInjector.service'
import { ICssFilesInjectorService } from './Services/CssFilesInjectorService/ICssFilesInjector.service'

import { IThemesManager } from './Managers/ThemesManager/IThemes.manager'
import { ThemesManager } from './Managers/ThemesManager/Themes.manager'

import { IHttpService } from './Services/HttpService/IHttp.service'
import { HttpService } from './Services/HttpService/Http.service'

import { IProviderSettingsService } from './Services/ProviderSettingsService/IProviderSettings.service'
import { ProviderSettingsService } from './Services/ProviderSettingsService/ProviderSettings.service'

import { DataFlowManager } from './Managers/DataFlow.manager'


@NgModule({ 
imports:      [ BrowserModule,HttpModule,RouterModule.forRoot([{path: 'login',component: LoginWindowComponent},{path : 'main',component: MainWindowComponent}]) ], 
declarations: [ AppComponent,LoginWindowComponent,MainWindowComponent ], 
bootstrap:    [ AppComponent ],
providers:    [
                { provide : IAppConfigurationService , useClass: AppConfigurationService },
                { provide : ILoggerService , useClass : LoggerService },
                { provide : IPlatformApiService , useClass : PlatformApiService} ,
                { provide : ILightstreamerSubscriptionService,useClass : LightstreamerSubscriptionService},
                { provide : ICssFilesInjectorService,useClass : CssFilesInjectorService},
                { provide : IThemesManager , useClass : ThemesManager},
                { provide : IHttpService , useClass : HttpService},
                { provide : IProviderSettingsService , useClass : ProviderSettingsService},                
                { provide : DataFlowManager , useClass : DataFlowManager}
              ]
}) 

export class AppModule 
{ 
    constructor(private ls :ILightstreamerSubscriptionService ,private platformService : IPlatformApiService ,private appConfigs:IAppConfigurationService,private dataManager : DataFlowManager,private router : Router)
    {        
        this.dataManager.AllServicesInitializedStream.subscribe(()=>
        {
            this.router.navigate(['/login']);
        })
    }
} 