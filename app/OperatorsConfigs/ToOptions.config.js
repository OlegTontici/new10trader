//to modify only config object

var fs = require('fs-extra')
var defaultConfigsPath = './resources/app-config/app.config.json'

var config = 
{
  "OperatorLiveId": "36717705-C0B5-42B5-AD5D-5DEFB00B5456",
  "OperatorName": "ToOptions",
  "HasOwnTheme" : true
}

function overrideDefaultConfigs ()
{
    fs.readJson(defaultConfigsPath,(error,jsonObject)=>
    {
        var newConfigs = jsonObject;
        newConfigs.OperatorLiveId = config.OperatorLiveId;
        newConfigs.OperatorName = config.OperatorName;		
		newConfigs.HasOwnTheme = config.HasOwnTheme;
        fs.writeJson(defaultConfigsPath,newConfigs);
    });
};

overrideDefaultConfigs ();