//to modify only config object

var fs = require('fs-extra')
var defaultConfigsPath = './resources/app-config/app.config.json'

var config = 
{
  "OperatorLiveId": "dc34751a-dad8-4822-95d4-56f613519e1f",
  "OperatorName": "TradePlus",
  "HasOwnTheme" : false
}

function overrideDefaultConfigs ()
{
    fs.readJson(defaultConfigsPath,(error,jsonObject)=>
    {
        var newConfigs = jsonObject;
        newConfigs.OperatorLiveId = config.OperatorLiveId;
        newConfigs.OperatorName = config.OperatorName;
		newConfigs.HasOwnTheme = config.HasOwnTheme;
        fs.writeJson(defaultConfigsPath,newConfigs);
    });
};

overrideDefaultConfigs ();