"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
var LoginWindow_component_1 = require("./Components/LoginWindowComponent/LoginWindow.component");
var MainWindow_component_1 = require("./Components/MainWindowComponent/MainWindow.component");
var IAppConfigurations_service_1 = require("./Services/AppConfigurationsService/IAppConfigurations.service");
var AppConfigurations_service_1 = require("./Services/AppConfigurationsService/AppConfigurations.service");
var ILogger_service_1 = require("./Services/LoggingService/ILogger.service");
var Logger_service_1 = require("./Services/LoggingService/Logger.service");
var IPlatformApi_service_1 = require("./Services/PlatformApiService/IPlatformApi.service");
var PlatformApi_service_1 = require("./Services/PlatformApiService/PlatformApi.service");
var LightstreamerSubscription_service_1 = require("./Services/LightstreamerSubscriptionService/LightstreamerSubscription.service");
var ILightstreamerSubscription_service_1 = require("./Services/LightstreamerSubscriptionService/ILightstreamerSubscription.service");
var CssFilesInjector_service_1 = require("./Services/CssFilesInjectorService/CssFilesInjector.service");
var ICssFilesInjector_service_1 = require("./Services/CssFilesInjectorService/ICssFilesInjector.service");
var IThemes_manager_1 = require("./Managers/ThemesManager/IThemes.manager");
var Themes_manager_1 = require("./Managers/ThemesManager/Themes.manager");
var IHttp_service_1 = require("./Services/HttpService/IHttp.service");
var Http_service_1 = require("./Services/HttpService/Http.service");
var IProviderSettings_service_1 = require("./Services/ProviderSettingsService/IProviderSettings.service");
var ProviderSettings_service_1 = require("./Services/ProviderSettingsService/ProviderSettings.service");
var DataFlow_manager_1 = require("./Managers/DataFlow.manager");
var AppModule = (function () {
    function AppModule(ls, platformService, appConfigs, dataManager, router) {
        var _this = this;
        this.ls = ls;
        this.platformService = platformService;
        this.appConfigs = appConfigs;
        this.dataManager = dataManager;
        this.router = router;
        this.dataManager.AllServicesInitializedStream.subscribe(function () {
            _this.router.navigate(['/login']);
        });
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, http_1.HttpModule, router_1.RouterModule.forRoot([{ path: 'login', component: LoginWindow_component_1.LoginWindowComponent }, { path: 'main', component: MainWindow_component_1.MainWindowComponent }])],
        declarations: [app_component_1.AppComponent, LoginWindow_component_1.LoginWindowComponent, MainWindow_component_1.MainWindowComponent],
        bootstrap: [app_component_1.AppComponent],
        providers: [
            { provide: IAppConfigurations_service_1.IAppConfigurationService, useClass: AppConfigurations_service_1.AppConfigurationService },
            { provide: ILogger_service_1.ILoggerService, useClass: Logger_service_1.LoggerService },
            { provide: IPlatformApi_service_1.IPlatformApiService, useClass: PlatformApi_service_1.PlatformApiService },
            { provide: ILightstreamerSubscription_service_1.ILightstreamerSubscriptionService, useClass: LightstreamerSubscription_service_1.LightstreamerSubscriptionService },
            { provide: ICssFilesInjector_service_1.ICssFilesInjectorService, useClass: CssFilesInjector_service_1.CssFilesInjectorService },
            { provide: IThemes_manager_1.IThemesManager, useClass: Themes_manager_1.ThemesManager },
            { provide: IHttp_service_1.IHttpService, useClass: Http_service_1.HttpService },
            { provide: IProviderSettings_service_1.IProviderSettingsService, useClass: ProviderSettings_service_1.ProviderSettingsService },
            { provide: DataFlow_manager_1.DataFlowManager, useClass: DataFlow_manager_1.DataFlowManager }
        ]
    }),
    __metadata("design:paramtypes", [ILightstreamerSubscription_service_1.ILightstreamerSubscriptionService, IPlatformApi_service_1.IPlatformApiService, IAppConfigurations_service_1.IAppConfigurationService, DataFlow_manager_1.DataFlowManager, router_1.Router])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map