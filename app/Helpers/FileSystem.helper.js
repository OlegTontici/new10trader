"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var FileSystem = (function () {
    function FileSystem() {
    }
    FileSystem.CopyAsync = function (source, desitnation) {
        fs.copy(source, desitnation, function (error) {
            if (error)
                return console.error(error);
            console.log("success!");
        });
    };
    FileSystem.FileExists = function (path) {
        return false;
    };
    FileSystem.ReadJsonAsync = function (path) {
        var responseStream = new rxjs_1.Subject();
        fs.readJson(path, function (error, object) {
            if (error)
                console.error(error);
            responseStream.next(object);
        });
        return responseStream;
    };
    FileSystem.WriteJsonAsync = function (path, objectToWrite) {
        fs.writeJson(path, objectToWrite, function (error) { return console.log(error); });
    };
    FileSystem.ReadFileAsync = function (path) {
        var responseStream = new rxjs_1.Subject();
        fs.readFile(path, function (error, object) {
            responseStream.next(object);
        });
        return responseStream;
    };
    FileSystem.WriteFileAsync = function (path, objectToWrite) {
        fs.writeFile(path, objectToWrite, function (error) { return console.error(error); });
    };
    FileSystem.GetFiles = function (folderPath) {
        var responseStream = new rxjs_1.Subject();
        fs.readdir(folderPath, function (err, data) {
            if (err)
                console.error(err);
            responseStream.next(data);
        });
        return responseStream;
    };
    return FileSystem;
}());
exports.FileSystem = FileSystem;
//# sourceMappingURL=FileSystem.helper.js.map