import { Subject } from 'rxjs'
declare var fs : any;

export class FileSystem
{
        public static CopyAsync(source: string,desitnation:string)
        {
            fs.copy(source,desitnation,(error) => 
            {
                if (error) return console.error(error);
                 
                console.log("success!");
            });
        }

        public static FileExists(path) : boolean
        {
            return false;
        }
        
        public static ReadJsonAsync(path) : Subject<any> 
        {
            let responseStream : Subject<any> = new Subject<any>();
            fs.readJson(path,(error,object) => 
            {
               if(error) 
                console.error(error);

                responseStream.next(object);
            });
            return responseStream;
        }

        public static WriteJsonAsync(path,objectToWrite)
        {
            fs.writeJson(path, objectToWrite, (error) => console.log(error))
        }

        public static ReadFileAsync(path): Subject<any> 
        {
            let responseStream : Subject<any> = new Subject<any>();
            fs.readFile(path,(error,object) => 
            {
                responseStream.next(object);
            });
            return responseStream;
        }

        public static WriteFileAsync(path,objectToWrite)
        {
            fs.writeFile(path, objectToWrite,(error) => console.error(error));
        }

        public static GetFiles(folderPath : string) : Subject<string[]>
        {
            let responseStream : Subject<string[]> = new Subject<string[]>();
            fs.readdir(folderPath,(err,data) => 
            {
                if(err) 
                console.error(err);

                responseStream.next(data)
            });
            return responseStream;
        }
}