import { Component } from '@angular/core'

@Component(
    {        
        selector : 'mainWindow',
        moduleId: module.id,
        templateUrl : 'MainWindow.component.html',
        styleUrls: ['MainWindow.component.css']
    })
export class MainWindowComponent
{
    sometext : string  = "Hello From Main Windows";
}