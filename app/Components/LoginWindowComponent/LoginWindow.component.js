"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var IThemes_manager_1 = require("../../Managers/ThemesManager/IThemes.manager");
var IPlatformApi_service_1 = require("../../Services/PlatformApiService/IPlatformApi.service");
var ILightstreamerSubscription_service_1 = require("../../Services/LightstreamerSubscriptionService/ILightstreamerSubscription.service");
var requests = require("../../Models/Requests");
var DataFlow_manager_1 = require("../../Managers/DataFlow.manager");
var LoginWindowComponent = (function () {
    function LoginWindowComponent(dataManager, ls, platformService, router, themesManager) {
        var _this = this;
        this.dataManager = dataManager;
        this.ls = ls;
        this.platformService = platformService;
        this.router = router;
        this.themesManager = themesManager;
        this.dataManager.ApplicationConfigurationsStream.subscribe(function (configs) {
            _this.loginWindowContent = configs.OperatorName;
            debugger;
            var LsData = configs.LighStreamerConnectionData;
            _this.ls.Subscribe(LsData.LSSubscriptionMode, LsData.LSItems, LsData.LSFields).subscribe(function (updateItem) { return console.log(updateItem); });
        });
    }
    LoginWindowComponent.prototype.ngOnInit = function () {
        this.themesManager.ApplyThemeForComponent('loginWindow');
    };
    LoginWindowComponent.prototype.btnOnClick = function () {
        this.platformService.IsOperatorSiteActive(new requests.IsOperatorSiteActiveRequestObject('ToOptions')).subscribe(function (response) { return console.log(response); }, function (error) { return console.log(error); });
        this.router.navigate(['/main']);
    };
    return LoginWindowComponent;
}());
LoginWindowComponent = __decorate([
    core_1.Component({
        selector: 'loginWindow',
        moduleId: module.id,
        templateUrl: 'LoginWindow.component.html',
        styleUrls: ['LoginWindow.component.css'],
        encapsulation: core_1.ViewEncapsulation.None
    }),
    __metadata("design:paramtypes", [DataFlow_manager_1.DataFlowManager, ILightstreamerSubscription_service_1.ILightstreamerSubscriptionService, IPlatformApi_service_1.IPlatformApiService, router_1.Router, IThemes_manager_1.IThemesManager])
], LoginWindowComponent);
exports.LoginWindowComponent = LoginWindowComponent;
//# sourceMappingURL=LoginWindow.component.js.map