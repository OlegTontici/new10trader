import { Component,ViewEncapsulation,OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { IThemesManager } from '../../Managers/ThemesManager/IThemes.manager'

import { IPlatformApiService } from '../../Services/PlatformApiService/IPlatformApi.service'
import { ILightstreamerSubscriptionService } from '../../Services/LightstreamerSubscriptionService/ILightstreamerSubscription.service'
import * as requests from '../../Models/Requests'

import { DataFlowManager } from '../../Managers/DataFlow.manager'

@Component(
    {
        selector: 'loginWindow',
        moduleId: module.id,
        templateUrl: 'LoginWindow.component.html',
        styleUrls : ['LoginWindow.component.css'],
        encapsulation : ViewEncapsulation.None
    })
    
export class LoginWindowComponent implements OnInit
{
    private loginWindowContent:string;
    
    constructor(private dataManager: DataFlowManager, private ls :ILightstreamerSubscriptionService ,private platformService : IPlatformApiService ,private router : Router,private themesManager : IThemesManager)
    {      
        this.dataManager.ApplicationConfigurationsStream.subscribe(
            configs => 
            {
                this.loginWindowContent = configs.OperatorName;

                let LsData = configs.LighStreamerConnectionData;
                this.ls.Subscribe(LsData.LSSubscriptionMode,LsData.LSItems,LsData.LSFields).subscribe(updateItem => console.log(updateItem));
            });        
    }

    ngOnInit(): void
    {
        this.themesManager.ApplyThemeForComponent('loginWindow');
    }

    btnOnClick() : void 
    {
        this.platformService.IsOperatorSiteActive(new requests.IsOperatorSiteActiveRequestObject('ToOptions')).subscribe(
            response => console.log(response),
            error => console.log(error)
        );
        
        this.router.navigate(['/main']);       
    }
}