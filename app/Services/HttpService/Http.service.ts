import { Http, Response } from '@angular/http';
import { Injectable} from '@angular/core'
import { Observable } from 'rxjs';

import { IHttpService } from './IHttp.service'

const maxRetryCount : number = 2;

@Injectable()
export class HttpService implements IHttpService
{
    private requestsHeader : any = 'Content-Type: application/json; charset=utf-8';

    constructor(private httpClient : Http)
    {
        
    }

    public Post<TIn,TOut>(endPoint : string,requestObject : TIn) : Observable<TOut>
    {
        return this.httpClient
                   .post(endPoint,requestObject,{headers : this.requestsHeader})
                   .map((response : Response) => <TOut> response.json())
                   .retryWhen(this.retryStrategy());
    }

    private retryStrategy()
    {
        return errors  => 
        {
           return errors.scan((acc,err) =>
            {
                if(acc > maxRetryCount)
                {
                    throw err;
                }          

                return acc + 1;
            },1).delay(1000)
        };
    }
}