"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CssFilesInjectorService = (function () {
    function CssFilesInjectorService() {
    }
    CssFilesInjectorService.prototype.Inject = function (fileName, operatorName, componentName) {
        // Create link
        var link = document.createElement('link');
        link.href = './resources/' + operatorName + '/css/' + fileName;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        var head = document.getElementsByTagName(componentName)[0];
        var links = head.getElementsByTagName('link');
        var style = head.getElementsByTagName('style')[0];
        // Check if the same style sheet has been loaded already.
        var isLoaded = false;
        for (var i = 0; i < links.length; i++) {
            var node = links[i];
            if (node.href.indexOf(link.href) > -1) {
                isLoaded = true;
            }
        }
        if (isLoaded)
            return;
        head.insertBefore(link, style).addEventListener('error', function (error) {
            console.log(error);
        });
    };
    return CssFilesInjectorService;
}());
exports.CssFilesInjectorService = CssFilesInjectorService;
//# sourceMappingURL=CssFilesInjector.service.js.map