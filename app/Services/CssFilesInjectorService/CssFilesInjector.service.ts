import { ICssFilesInjectorService } from './ICssFilesInjector.service'
export class CssFilesInjectorService implements ICssFilesInjectorService
{
    public Inject(fileName : string,operatorName :string,componentName : string) : void
    {
         // Create link
        let link = document.createElement('link');
        link.href = './resources/' + operatorName + '/css/' + fileName;        
        link.rel = 'stylesheet';
        link.type = 'text/css';
        
        let head = document.getElementsByTagName(componentName)[0];
        let links = head.getElementsByTagName('link');
        let style = head.getElementsByTagName('style')[0];
        
        // Check if the same style sheet has been loaded already.
        let isLoaded = false;  
        for (var i = 0; i < links.length; i++) 
        {
            var node = links[i];
            if (node.href.indexOf(link.href) > -1) 
            {
                isLoaded = true;
            }
        }
        
        if (isLoaded) return;

        head.insertBefore(link, style).addEventListener('error',(error)=> 
        {
            console.log(error);            
        });
    }
}