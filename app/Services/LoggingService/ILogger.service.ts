import { Injectable} from '@angular/core'

@Injectable()
export abstract class ILoggerService
{
    public Info(message : string) :void
    {

    }

    public Error(error : Error,message : string) : void
    {

    } 

     public Fatal(error : Error,message : string) : void
    {

    } 
}