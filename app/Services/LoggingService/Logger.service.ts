import { ILoggerService } from './ILogger.service'
import { Injectable} from '@angular/core'

@Injectable()
export class LoggerService implements ILoggerService
{
    public Info(message : string) :void
    {
        console.log(message);
    }

    public Error(error : Error,message : string) : void
    {
        console.log(error);
    } 

     public Fatal(error : Error,message : string) : void
    {
        console.log(error);
    } 
}