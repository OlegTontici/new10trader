import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'

import { GetProviderSettingsRequestObject } from '../../Models/Requests'
import { IProviderSettingsService } from './IProviderSettings.service'
import { IHttpService } from '../../Services/HttpService/IHttp.service'

@Injectable()
export class ProviderSettingsService implements IProviderSettingsService
{
    constructor(private httpClient : IHttpService)
    {

    }
    public GetSettings(apiEndPoint : string,requestObject : GetProviderSettingsRequestObject) : Observable<any>
    {
        return this.httpClient.Post<GetProviderSettingsRequestObject,any>(apiEndPoint,requestObject);
    }
}