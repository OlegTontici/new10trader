"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var DataFlow_manager_1 = require("../../Managers/DataFlow.manager");
var AppContext_model_1 = require("../../Models/AppContext.model");
var FileSystem_helper_1 = require("../../Helpers/FileSystem.helper");
var IProviderSettings_service_1 = require("../../Services/ProviderSettingsService/IProviderSettings.service");
var requests = require("../../Models/Requests");
var AppConfigurationService = (function () {
    function AppConfigurationService(providerSettingsService, appManager) {
        var _this = this;
        this.providerSettingsService = providerSettingsService;
        this.appManager = appManager;
        this.applicationContext = new AppContext_model_1.AppContextModel();
        this.configsPath = './resources/app-config/app.config.json';
        this.OnConfigSetStream = new rxjs_1.Subject();
        this.OnConfigSetStream.map(function (settings) {
            if (!_this.providerConfigurations) {
                _this.providerConfigurations = settings;
            }
            return _this.applicationContext.IsDemo ? settings.ProviderDemoConfiguration : settings.ProviderLiveConfiguration;
        }).subscribe(this.appManager.SetAppConfigs);
        this.LoadAppConfigurationSettings().subscribe(function (result) {
            _this.applicationContext = result;
            _this.providerSettingsService.GetSettings(_this.applicationContext.ProviderSettingsApiUrl, new requests.GetProviderSettingsRequestObject(_this.applicationContext.OperatorLiveId))
                .subscribe(function (configs) { return _this.OnConfigSetStream.next(configs); }, function (error) {
                console.log(error);
                /*load data from local storage*/
            });
        });
    }
    AppConfigurationService.prototype.SaveAppConfigurationSettings = function () {
        FileSystem_helper_1.FileSystem.WriteJsonAsync(this.configsPath, this.providerConfigurations);
    };
    AppConfigurationService.prototype.LoadAppConfigurationSettings = function () {
        return FileSystem_helper_1.FileSystem.ReadJsonAsync(this.configsPath);
    };
    return AppConfigurationService;
}());
AppConfigurationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [IProviderSettings_service_1.IProviderSettingsService, DataFlow_manager_1.DataFlowManager])
], AppConfigurationService);
exports.AppConfigurationService = AppConfigurationService;
//# sourceMappingURL=AppConfigurations.service.js.map