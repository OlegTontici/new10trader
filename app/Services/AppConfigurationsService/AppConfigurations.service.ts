import { Injectable} from '@angular/core'
import { Subject } from 'rxjs'

import { DataFlowManager } from '../../Managers/DataFlow.manager'
import { IAppConfigurationService } from './IAppConfigurations.service' 
import { ProviderConfigurationModel } from '../../Models/ProviderConfiguration.model'
import { ProviderConfigurationsModel } from '../../Models/ProviderConfigurations.model'
import { AppContextModel } from '../../Models/AppContext.model'
import { FileSystem } from '../../Helpers/FileSystem.helper'
import { IProviderSettingsService } from '../../Services/ProviderSettingsService/IProviderSettings.service'
import * as requests from '../../Models/Requests'



@Injectable()
export class AppConfigurationService implements  IAppConfigurationService
{
    private providerConfigurations : ProviderConfigurationsModel;
    private applicationContext : AppContextModel = new AppContextModel();
    private configsPath : string = './resources/app-config/app.config.json'
    private OnConfigSetStream : Subject<ProviderConfigurationsModel> = new Subject<ProviderConfigurationsModel>(); 

    constructor(private providerSettingsService : IProviderSettingsService,private appManager : DataFlowManager)
    {
        this.OnConfigSetStream.map(settings => 
        {            
            if(!this.providerConfigurations)
            {
                this.providerConfigurations = settings;
            }
            
            return this.applicationContext.IsDemo ? settings.ProviderDemoConfiguration : settings.ProviderLiveConfiguration;

        }).subscribe(this.appManager.SetAppConfigs);   

        this.LoadAppConfigurationSettings().subscribe(result => 
        {   
            this.applicationContext = result;

            this.providerSettingsService.GetSettings(this.applicationContext.ProviderSettingsApiUrl,new requests.GetProviderSettingsRequestObject(this.applicationContext.OperatorLiveId))
                                        .subscribe(
                                            configs => this.OnConfigSetStream.next(configs),
                                            error => 
                                                {
                                                    console.log(error);
                                                    /*load data from local storage*/
                                                }
            );
        });     
    }

    private SaveAppConfigurationSettings() : void
    {
        FileSystem.WriteJsonAsync(this.configsPath,this.providerConfigurations);        
    }

    private LoadAppConfigurationSettings() : Subject<any>
    {   
        return FileSystem.ReadJsonAsync(this.configsPath);        
    }
}