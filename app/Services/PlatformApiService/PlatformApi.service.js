"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var DataFlow_manager_1 = require("../../Managers/DataFlow.manager");
var IHttp_service_1 = require("../HttpService/IHttp.service");
var PlatformApiService = (function () {
    function PlatformApiService(httpClient, dataManager) {
        var _this = this;
        this.httpClient = httpClient;
        this.dataManager = dataManager;
        this.setConfigs = new rxjs_1.Subject();
        this.setConfigs.map(function (settings) { return _this.platformApiURL = settings.PlatformApiConnectionData.PlatformApiURL; })
            .subscribe(this.dataManager.ServiceInitializedStream);
        this.dataManager.ApplicationConfigurationsStream.subscribe(this.setConfigs);
    }
    PlatformApiService.prototype.Login = function (param) {
        return this.Post('Login', param);
    };
    PlatformApiService.prototype.Logout = function (param) {
        return this.Post('Logout', param);
    };
    PlatformApiService.prototype.QuickDemoLogin = function (param) {
        return this.Post('QuickDemoLogin', param);
    };
    PlatformApiService.prototype.GetToken = function (param) {
        return this.Post('GetToken', param);
    };
    PlatformApiService.prototype.Buy = function (param) {
        return this.Post('Buy', param);
    };
    PlatformApiService.prototype.Sell = function (param) {
        return this.Post('Sell', param);
    };
    PlatformApiService.prototype.CancelTradeAction = function (param) {
        return this.Post('CancelTradeAction', param);
    };
    PlatformApiService.prototype.GetTraderBalance = function (param) {
        return this.Post('GetTraderBalance', param);
    };
    PlatformApiService.prototype.GetTraderParams = function (param) {
        return this.Post('GetTraderParams', param);
    };
    PlatformApiService.prototype.GetReport = function (param) {
        return this.Post('GetReport', param);
    };
    PlatformApiService.prototype.IsOperatorSiteActive = function (param) {
        return this.Post('IsOperatorSiteActive', param);
    };
    PlatformApiService.prototype.GetRecentHistoricMarketData = function (param) {
        return this.Post('GetRecentHistoricMarketData', param);
    };
    PlatformApiService.prototype.GetTradeActionCancellationInformation = function (param) {
        return this.Post('GetTradeActionCancellationInformation', param);
    };
    PlatformApiService.prototype.Post = function (apiEndPoint, requestObject) {
        return this.httpClient.Post(this.platformApiURL + apiEndPoint, requestObject);
    };
    return PlatformApiService;
}());
PlatformApiService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [IHttp_service_1.IHttpService, DataFlow_manager_1.DataFlowManager])
], PlatformApiService);
exports.PlatformApiService = PlatformApiService;
//# sourceMappingURL=PlatformApi.service.js.map