import { Injectable} from '@angular/core'
import { Observable,Subject } from 'rxjs'

import { IPlatformApiService } from './IPlatformApi.service'
import { DataFlowManager } from '../../Managers/DataFlow.manager'

import { IHttpService } from '../HttpService/IHttp.service'
import { ProviderConfigurationModel } from '../../Models/ProviderConfiguration.model'

import { LoginRequestObject,
            GetTraderParamsRequestObject,
            LogoutRequestObject,
            QuickDemoLoginRequestObject,
            GetTokenRequestObject,
            BuyRequestObject,
            SellRequestObject,
            CancelTradeActionRequestObject,
            GetTraderBalanceRequestObject,
            GetReportRequestObject ,
            IsOperatorSiteActiveRequestObject,
            GetRecentHistoricMarketDataRequestObject,
            GetTradeActionCancellationInformationRequestObject} from '../../Models/Requests'

import { LoginResponseObject,
            QuickDemoLoginResponseObject,
            GetTokenResponseObject,
            GetTraderBalanceResponseObject,
            GetTraderParamsResponseObject,
            GetReportResponseObject,
            IsOperatorSiteActiveResponseObject,
            GetRecentHistoricMarketDataResponseObject,
            GetTradeActionCancellationInformationResponseObject,
            BuyResponseObject,
            GenericResponse} from '../../Models/Responses'

@Injectable()
export class PlatformApiService implements IPlatformApiService
{   
    private platformApiURL : string;    
    private setConfigs : Subject<ProviderConfigurationModel> = new Subject<ProviderConfigurationModel>();

    constructor(private httpClient : IHttpService,private dataManager : DataFlowManager)
    {        
        this.setConfigs.map(settings => this.platformApiURL = settings.PlatformApiConnectionData.PlatformApiURL)
                       .subscribe(this.dataManager.ServiceInitializedStream);
                       
        this.dataManager.ApplicationConfigurationsStream.subscribe(this.setConfigs);
    }

    public Login(param : LoginRequestObject) : Observable<GenericResponse<LoginResponseObject>>   
    {
        return this.Post<LoginRequestObject,LoginResponseObject>('Login',param);
    }

    public Logout(param : LogoutRequestObject) : Observable<GenericResponse<Object>> 
    {
        return this.Post<LogoutRequestObject,Object>('Logout',param);
    }

    public QuickDemoLogin(param : QuickDemoLoginRequestObject) : Observable<GenericResponse<QuickDemoLoginResponseObject>>  
    {
        return this.Post<QuickDemoLoginRequestObject,QuickDemoLoginResponseObject>('QuickDemoLogin',param);
    }

    public GetToken(param : GetTokenRequestObject) : Observable<GenericResponse<GetTokenResponseObject>>  
    {
        return this.Post<GetTokenRequestObject,GetTokenResponseObject>('GetToken',param);
    }

    public Buy (param : BuyRequestObject) :  Observable<GenericResponse<BuyResponseObject>> 
    {
        return this.Post<BuyRequestObject,BuyResponseObject>('Buy',param);
    }

    public Sell(param : SellRequestObject) : Observable<GenericResponse<Object>> 
    {
        return this.Post<SellRequestObject,Object>('Sell',param);
    }

    public CancelTradeAction (param : CancelTradeActionRequestObject) : Observable<GenericResponse<Object>>
    {
        return this.Post<CancelTradeActionRequestObject,Object>('CancelTradeAction',param);
    }

    public GetTraderBalance (param : GetTraderBalanceRequestObject): Observable<GenericResponse<GetTraderBalanceResponseObject>>   
    {
        return this.Post<GetTraderBalanceRequestObject,GetTraderBalanceResponseObject>('GetTraderBalance',param);
    }

    public GetTraderParams(param : GetTraderParamsRequestObject): Observable<GenericResponse<GetTraderParamsResponseObject>>    
    {
        return this.Post<GetTraderParamsRequestObject,GetTraderParamsResponseObject>('GetTraderParams',param);   
    }

    public GetReport(param : GetReportRequestObject) : Observable<GenericResponse<GetReportResponseObject>>   
    {
        return this.Post<GetReportRequestObject,GetReportResponseObject>('GetReport',param);   
    }

    public IsOperatorSiteActive(param : IsOperatorSiteActiveRequestObject) : Observable<GenericResponse<IsOperatorSiteActiveResponseObject>>
    {
        return this.Post<IsOperatorSiteActiveRequestObject,IsOperatorSiteActiveResponseObject>('IsOperatorSiteActive',param);       
    }

    public GetRecentHistoricMarketData(param : GetRecentHistoricMarketDataRequestObject) : Observable<GenericResponse<GetRecentHistoricMarketDataResponseObject>> 
    {
       return this.Post<GetRecentHistoricMarketDataRequestObject,GetRecentHistoricMarketDataResponseObject>('GetRecentHistoricMarketData',param);
    } 

    public GetTradeActionCancellationInformation (param : GetTradeActionCancellationInformationRequestObject) : Observable<GenericResponse<GetTradeActionCancellationInformationResponseObject>> 
    {
        return this.Post<GetTradeActionCancellationInformationRequestObject,GetTradeActionCancellationInformationResponseObject>('GetTradeActionCancellationInformation',param);
    }

    public Post<TIn,TOut> (apiEndPoint : string , requestObject : TIn) : Observable<GenericResponse<TOut>>
    {      
       return this.httpClient.Post<TIn,GenericResponse<TOut>>(this.platformApiURL + apiEndPoint,requestObject);
    }
}