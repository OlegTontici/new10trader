"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var IPlatformApiService = (function () {
    function IPlatformApiService() {
    }
    IPlatformApiService.prototype.Login = function (param) {
        return null;
    };
    IPlatformApiService.prototype.Logout = function (param) {
        return null;
    };
    IPlatformApiService.prototype.QuickDemoLogin = function (param) {
        return null;
    };
    IPlatformApiService.prototype.GetToken = function (param) {
        return null;
    };
    IPlatformApiService.prototype.Buy = function (param) {
        return null;
    };
    IPlatformApiService.prototype.Sell = function (param) {
    };
    IPlatformApiService.prototype.CancelTradeAction = function (param) {
    };
    IPlatformApiService.prototype.GetTraderBalance = function (param) {
        return null;
    };
    IPlatformApiService.prototype.GetTraderParams = function (param) {
        return null;
    };
    IPlatformApiService.prototype.GetReport = function (param) {
        return null;
    };
    IPlatformApiService.prototype.IsOperatorSiteActive = function (param) {
        return null;
    };
    IPlatformApiService.prototype.GetRecentHistoricMarketData = function (param) {
        return null;
    };
    IPlatformApiService.prototype.GetTradeActionCancellationInformation = function (param) {
        return null;
    };
    IPlatformApiService.prototype.Post = function (apiEndPoint, requestObject) {
        return null;
    };
    return IPlatformApiService;
}());
IPlatformApiService = __decorate([
    core_1.Injectable()
], IPlatformApiService);
exports.IPlatformApiService = IPlatformApiService;
//# sourceMappingURL=IPlatformApi.service.js.map