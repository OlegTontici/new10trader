import { Injectable} from '@angular/core'
import { Observable } from 'rxjs/Observable';

import { LoginRequestObject,
            GetTraderParamsRequestObject,
            LogoutRequestObject,
            QuickDemoLoginRequestObject,
            GetTokenRequestObject,
            BuyRequestObject,
            SellRequestObject,
            CancelTradeActionRequestObject,
            GetTraderBalanceRequestObject,
            GetReportRequestObject ,
            IsOperatorSiteActiveRequestObject,
            GetRecentHistoricMarketDataRequestObject,
            GetTradeActionCancellationInformationRequestObject} from '../../Models/Requests'

import { LoginResponseObject,
            QuickDemoLoginResponseObject,
            GetTokenResponseObject,
            GetTraderBalanceResponseObject,
            GetTraderParamsResponseObject,
            GetReportResponseObject,
            IsOperatorSiteActiveResponseObject,
            GetRecentHistoricMarketDataResponseObject,
            GetTradeActionCancellationInformationResponseObject,
            GenericResponse,
            BuyResponseObject} from '../../Models/Responses'

@Injectable()
export abstract class IPlatformApiService
{
    public Login(param : LoginRequestObject) : Observable<GenericResponse<LoginResponseObject>> 
    {
        return null;
    }

    public Logout(param : LogoutRequestObject) : Observable<GenericResponse<Object>> 
    {
        return null;
    }

    public QuickDemoLogin(param : QuickDemoLoginRequestObject) : Observable<GenericResponse<QuickDemoLoginResponseObject>>   
    {
        return null;
    }

    public GetToken(param : GetTokenRequestObject) : Observable<GenericResponse<GetTokenResponseObject>>  
    {
        return null;
    }

    public Buy (param : BuyRequestObject) : Observable<GenericResponse<BuyResponseObject>> 
    {
        return null;
    }

    public Sell(param : SellRequestObject) : void
    {

    }

    public CancelTradeAction (param : CancelTradeActionRequestObject) : void
    {

    }

    public GetTraderBalance (param : GetTraderBalanceRequestObject): Observable<GenericResponse<GetTraderBalanceResponseObject>>  
    {
        return null;
    }

    public GetTraderParams(param : GetTraderParamsRequestObject): Observable<GenericResponse<GetTraderParamsResponseObject>>   
    {
        return null;
    }

    public GetReport(param : GetReportRequestObject) : Observable<GenericResponse<GetReportResponseObject>>  
    {
        return null;
    }

    public IsOperatorSiteActive(param : IsOperatorSiteActiveRequestObject) : Observable<GenericResponse<IsOperatorSiteActiveResponseObject>>
    {
        return null;
    }

    public GetRecentHistoricMarketData(param : GetRecentHistoricMarketDataRequestObject) : Observable<GenericResponse<GetRecentHistoricMarketDataResponseObject>>
    {
        return null;
    } 

    public GetTradeActionCancellationInformation (param : GetTradeActionCancellationInformationRequestObject) : Observable<GenericResponse<GetTradeActionCancellationInformationResponseObject>>
    {
        return null;
    }

     public Post<Tin,Tout> (apiEndPoint : string,requestObject : Tin) : Observable<Tout>
    {
        return null;
    }
}