import * as _ from 'lodash'
import { Injectable} from '@angular/core'
import { Subject } from 'rxjs'
import { ILightstreamerSubscriptionService } from './ILightstreamerSubscription.service'
import { DataFlowManager } from '../../Managers/DataFlow.manager'
import { ProviderConfigurationModel } from '../../Models/ProviderConfiguration.model'

declare var LightstreamerClient: any;
declare var Subscription: any;

@Injectable()
export class LightstreamerSubscriptionService implements ILightstreamerSubscriptionService
{
    private lsClient;
    private subscribedItems: Array<SubscriptionItem> = Array<SubscriptionItem>();
    private Connect : Subject<ProviderConfigurationModel> = new Subject<ProviderConfigurationModel>();

    constructor(private dataManager : DataFlowManager)
    {
        this.Connect.map(configs =>
        {
            this.lsClient = new LightstreamerClient(configs.LighStreamerConnectionData.LSServerURL,configs.LighStreamerConnectionData.LSAdapterSet);
            this.lsClient.connect();
            this.lsClient.addListener(
            {
                onStatusChange : (currentStatus) => console.log(currentStatus),
                onServerError : (error) => console.log(error)
            });

            return true;
        }).subscribe(this.dataManager.ServiceInitializedStream);

        this.dataManager.ApplicationConfigurationsStream.subscribe(this.Connect);
    }

    /**
     * Creates an instance of Subscription type(see LS documentation) with given parameters and subscribe this instance to lighstreamer client.
     * >subscribeKey - optional parameter.When have value,created subscription object is stored into an array with given key.
     * This array is used in "UnSubcribe" method for identifing object that need to be unsubscribed
     */
    public Subscribe(subscriptionMode : string, items : string[], fields : string[],subscribeKey ? : string) : Subject<any>
    {        
        let updateStream = new Subject<any>();
        let subscriptionObject = new Subscription(subscriptionMode,items,fields);
        subscriptionObject.setDataAdapter("QUOTE_ADAPTER");
        subscriptionObject.setRequestedSnapshot("yes");
        subscriptionObject.setRequestedMaxFrequency(1);
        subscriptionObject.addListener(
        {
            onItemUpdate : (updateItem : any) => updateStream.next(updateItem)
        });

        this.lsClient.subscribe(subscriptionObject);
        
        if(subscribeKey != null ) this.subscribedItems.push(new SubscriptionItem(subscribeKey,subscriptionObject));

        return updateStream;
    }

    /**
     * Unsubscribe a subscription from lighstreamer client
     */
    public UnSubcribe(key : string)
    {
        var itemToUnsubscribe = _.find(this.subscribedItems,item=> item.key == key);
        if(itemToUnsubscribe != null) this.lsClient.unsubscribe(itemToUnsubscribe.value);
    }
}

class SubscriptionItem
{
    constructor(key : string, val : Object)
    {
        this.key = key;
        this.value = val;
    }
    public key : string;
    public value : Object;
}