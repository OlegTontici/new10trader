"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var DataFlow_manager_1 = require("../../Managers/DataFlow.manager");
var LightstreamerSubscriptionService = (function () {
    function LightstreamerSubscriptionService(dataManager) {
        var _this = this;
        this.dataManager = dataManager;
        this.subscribedItems = Array();
        this.Connect = new rxjs_1.Subject();
        this.Connect.map(function (configs) {
            _this.lsClient = new LightstreamerClient(configs.LighStreamerConnectionData.LSServerURL, configs.LighStreamerConnectionData.LSAdapterSet);
            _this.lsClient.connect();
            _this.lsClient.addListener({
                onStatusChange: function (currentStatus) { return console.log(currentStatus); },
                onServerError: function (error) { return console.log(error); }
            });
            return true;
        }).subscribe(this.dataManager.ServiceInitializedStream);
        this.dataManager.ApplicationConfigurationsStream.subscribe(this.Connect);
    }
    /**
     * Creates an instance of Subscription type(see LS documentation) with given parameters and subscribe this instance to lighstreamer client.
     * >subscribeKey - optional parameter.When have value,created subscription object is stored into an array with given key.
     * This array is used in "UnSubcribe" method for identifing object that need to be unsubscribed
     */
    LightstreamerSubscriptionService.prototype.Subscribe = function (subscriptionMode, items, fields, subscribeKey) {
        var updateStream = new rxjs_1.Subject();
        var subscriptionObject = new Subscription(subscriptionMode, items, fields);
        subscriptionObject.setDataAdapter("QUOTE_ADAPTER");
        subscriptionObject.setRequestedSnapshot("yes");
        subscriptionObject.setRequestedMaxFrequency(1);
        subscriptionObject.addListener({
            onItemUpdate: function (updateItem) { return updateStream.next(updateItem); }
        });
        this.lsClient.subscribe(subscriptionObject);
        if (subscribeKey != null)
            this.subscribedItems.push(new SubscriptionItem(subscribeKey, subscriptionObject));
        return updateStream;
    };
    /**
     * Unsubscribe a subscription from lighstreamer client
     */
    LightstreamerSubscriptionService.prototype.UnSubcribe = function (key) {
        var itemToUnsubscribe = _.find(this.subscribedItems, function (item) { return item.key == key; });
        if (itemToUnsubscribe != null)
            this.lsClient.unsubscribe(itemToUnsubscribe.value);
    };
    return LightstreamerSubscriptionService;
}());
LightstreamerSubscriptionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [DataFlow_manager_1.DataFlowManager])
], LightstreamerSubscriptionService);
exports.LightstreamerSubscriptionService = LightstreamerSubscriptionService;
var SubscriptionItem = (function () {
    function SubscriptionItem(key, val) {
        this.key = key;
        this.value = val;
    }
    return SubscriptionItem;
}());
//# sourceMappingURL=LightstreamerSubscription.service.js.map